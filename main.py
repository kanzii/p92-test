from typing import Union
from fastapi import FastAPI, WebSocket
from fastapi.responses import RedirectResponse, HTMLResponse
import uvicorn
from pydantic import BaseModel
import ws_client
import asyncio
import logging

app = FastAPI()

main_dict = {}

#test_page
@app.get("/hello")
def read_root():
    """
    Welcome page.
    """
    return {"Hello": "P92"}

@app.post("/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    """
    Send data to dictionary
    """
    main_dict['item_id'] = item_id
    main_dict['q'] = q
    ws_run = ws_client.hello(main_dict)
    logging.info(main_dict)
    asyncio.run(ws_run)
    return RedirectResponse("/app/ui")

@app.post("/app/ui/")
async def redirect_item():
    """
    Receive data...
    """
    item_id = main_dict['item_id']
    q = main_dict['q']
    return {"item_id": item_id, "q": q}

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8080)
