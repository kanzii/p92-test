import websockets
import asyncio
import os
import logging

PORT = 8081

#log setup
logging.basicConfig(format='[%(asctime)s] %(levelname)-8s %(message)s',
					datefmt='%Y-%m-%d %H:%M:%S',
					level=os.environ.get("LOGLEVEL", "INFO"))

connected = set()

logging.info(f"WS server listening on port {PORT}")

async def handler(websocket, path):
	logging.info("Client connected")
	connected.add(websocket)
	try:
		async for message in websocket:
			logging.info(f"Recieved message from client: {message}")
			for conn in connected:
				if conn != websocket:
					await conn.send(f"Someone said {message}")
	except websockets.exceptions.ConnectionClosed as e:
		logging.warning(f"Client disconnected, {e}")
	finally:
		connected.remove(websocket)

start_server = websockets.serve(handler, "localhost", PORT)

if __name__ == '__main__':
	asyncio.get_event_loop().run_until_complete(start_server)
	asyncio.get_event_loop().run_forever()