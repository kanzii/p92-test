import websockets
import asyncio
import logging
import os
import json

#log setup
logging.basicConfig(format='[%(asctime)s] %(levelname)-8s %(message)s',
					datefmt='%Y-%m-%d %H:%M:%S',
					level=os.environ.get("LOGLEVEL", "INFO"))

URL =  "ws://127.0.0.1:8081"

async def listen(message: str):
	async with websockets.connect(URL) as ws:
		await ws.send(message)
		while True:
			msg = await ws.recv()
			logging.info(msg)

async def hello(msg):
	jmsg = json.dumps(msg)
	logging.debug(jmsg, type(jmsg))
	async with websockets.connect(URL) as ws:
		await ws.send(jmsg)
	ws.close()

if __name__ == '__main__':
	asyncio.get_event_loop().run_until_complete(listen("Bah!"))
