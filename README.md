# P92 test work

## Download
Download the environment `git clone https://gitlab.com/kanzii/p92-test.git`

## Installation
* create python virtual environment `python -m venv p92-test`
* Initialize the python virtual environment. In the downloaded git folder, run: `source ./bin/activate`
* install packages `pip install -r requirements.txt`

## Start
* Open terminal, and run the FastAPI server (in the git repo directory) `python main.py`
* Another terminal, run the websocket server `python ws_server.py`

## Usage
* in a web browser open `http://localhost:8080/docs`
* test it
* you can run manual test on `http://localhost:8080/docs` site. Docs, and tests in the same page.
* some automatic test is in the `api_test.sh` file, run in `bash`: `./api_test.sh`

## Close
* `Ctrl-C` in the CLI windows
* In CLI enter `deactivate` for close the python virtual environment
